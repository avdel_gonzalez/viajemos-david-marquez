<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cities')->insert([
            [
                'name' => 'Miami',
                'code_name' => 'miami',
                'country_id' => '1'
            ],
            [
                'name' => 'New York',
                'code_name' => 'nyc',
                'country_id' => '1'
            ],
            [
                'name' => 'Orlando',
                'code_name' => 'orlando',
                'country_id' => '1'
            ],
        ]);
    }
}
