PRUEBA DE CONOCIMIENTO DAVID FELIPE MÁRQUEZ GONZÁLEZ
<br /><br /><br />
Desarrollado con:<br />
<ul>
    <li>Laravel 7</li>
    <li>jQuery Datatables</li>
    <li>Bootstrap 4</li>
    <li>WEATHER Webservice</li>
</ul>
<br /><br />
Para comenzar, debe clonar el proyecto, en la carpeta raíz, ejecute:<br />
<tt>composer install</tt><br /><br />
Luego, deberá copiar el archivo .env.example de la siguiente forma:<br/>
<tt>cp .env.example .env</tt>
<br /><br />
Allí encontrará las siguientes variables (con sus valores listos)<br />
WEATHER_CONSUMER_KEY<br />
WEATHER_CONSUMER_SECRET<br />
WEATHER_APP_ID<br />
WEATHER_API_URL<br />
<br /><br />
Adicionalmente, cree una base de datos (MySql o PostgreSql) y modifique las siguintes variables en el archivo .env:<br />
Para MySql:<br /><br />
DB_CONNECTION=mysql<br />
DB_HOST=127.0.0.1<br />
DB_PORT=3306<br />
DB_DATABASE={Nombre de la base de datos}<br />
DB_USERNAME={Nombre de usuario}<br />
DB_PASSWORD={Contraseña}<br /><br />
Para PostgreSql:<br />
DB_CONNECTION=pgsql<br />
DB_HOST=localhost<br />
DB_PORT=5432<br />
DB_DATABASE={Nombre de la base de datos}<br />
DB_USERNAME={Nombre de Usuario}<br />
DB_PASSWORD={Contraseña}<br /><br />
Guarde, y luego ejecute <tt>php artisan key:generate</tt><br /><br />
A continuación, debe ejecutar las migraciones y sembradores de la base de datos. Para hacerlo con un solo comando, puede ejecutar: <br />
<tt>php artisan migrate --seed</tt><br /><br />
Finalmente, debe lanzar el servidor local para ver el proyecto funcionando:<br /><br />
<tt>php artisan serve</tt> 
