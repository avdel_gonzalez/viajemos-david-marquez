<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@index',
    'as' => 'home.index'
]);

Route::post('/searchCity', [
    'uses' => 'HomeController@searchCity',
    'as' => 'home.searchCity'
]);

Route::get('/historial', [
    'uses' => 'HistorialController@index',
    'as' => 'historial.index'
]);

Route::get('lang/{lang}', 'LanguageController@change')->name('lang.change');