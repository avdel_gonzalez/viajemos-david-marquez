<header class="topbar" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header" data-logobg="skin6">
            <a class="navbar-brand" href="{{ route('home.index') }}">
                <!-- Logo icon -->
                <b class="logo-icon">
                    <!-- Dark Logo icon -->
                    <img class="logo-icon-img" src="plugins/images/logo-icon.png" alt="homepage" />
                </b>
            </a>
            <a class="nav-toggler waves-effect waves-light text-dark d-block d-md-none"
                href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
        </div>
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <ul class="navbar-nav d-none d-md-block d-lg-none">
                <li class="nav-item">
                    <a class="nav-toggler nav-link waves-effect waves-light text-white"
                        href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto d-flex align-items-center">
                @if (config('locale.status') && count(config('locale.languages')) > 1)
                    <div class="text-right language-toogle">
                        <span class="change-lang-text">{{ __('messages.change_language') }}</span>
                        @foreach (array_keys(config('locale.languages')) as $lang)
                            <a href="{!! route('lang.change', $lang) !!}">
                                @switch($lang)
                                    @case('es')
                                        <img class="flags @if($lang == App::getLocale()) current-lang-flag @endif" src="{{ asset('flags/co.png') }}">
                                        @break
                                    @case('en')
                                        <img class="flags @if($lang == App::getLocale()) current-lang-flag @endif" src="{{ asset('flags/us.png') }}">
                                        @break
                                    @case('pt')
                                        <img class="flags @if($lang == App::getLocale()) current-lang-flag @endif" src="{{ asset('flags/br.png') }}">
                                        @break
                                @endswitch
                            </a>
                        @endforeach
                    </div>
                @endif
            </ul>
        </div>
    </nav>
</header>