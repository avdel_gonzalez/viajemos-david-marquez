<aside class="left-sidebar" data-sidebarbg="skin6">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <!-- User Profile-->
                <li class="sidebar-item pt-2">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('home.index') }}"
                        aria-expanded="false">
                        <i class="fas fa-map-marker-alt" aria-hidden="true"></i>
                        <span class="hide-menu">{{ __('messages.view_map') }}</span>
                    </a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link" href="{{ route('historial.index') }}"
                        aria-expanded="false">
                        <i class="fas fa-history" aria-hidden="true"></i>
                        <span class="hide-menu">{{ __('messages.historial') }}</span>
                    </a>
                </li>
            </ul
        </nav>
    </div>
</aside>