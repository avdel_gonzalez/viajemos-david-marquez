@extends('welcome')

@section('header_scripts')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
        integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
        crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
        integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
        crossorigin=""></script>
    <style>
        #mapid { height: 67vh; }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-9">
            {{ $data_view['city'] }}
        </div>
        <div class="col-md-3">
            <form class="form-inline" method="POST" action="{{ route('home.searchCity') }}">
                @csrf
                <div class="input-group">
                    <select class="form-control" name="city_id">
                        <option>{{ __('messages.select_city') }}</option>
                        @foreach($cities as $city)
                            <option value="{{ $city->id }}">{{ $city->name }} ({{ $city->country->name }})</option>
                        @endforeach
                    </select>
                </div>
                <button type="submit" class="btn btn-primary">
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>
    <br />
    <div id="mapid"></div>
@endsection

@section('footer_scripts')
    <script>
        let latitude = "{{ $data_view['latitude'] }}";
        let longitude = "{{ $data_view['longitude'] }}";
        let pop_up_text = `
            {{ __('messages.city') }}: {{ $data_view['city'] }}<br />
            {{ __('messages.timezone') }}: {{ $data_view['timezone_id'] }}<br />
            {{ __('messages.humidity') }}: {{ $data_view['humidity'] }}<br />
        `;
        var mymap = L.map('mapid').setView([latitude, longitude], 10);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(mymap);

        L.marker([latitude, longitude]).addTo(mymap)
        .bindPopup(pop_up_text)
        .openPopup();
    </script>
@endsection