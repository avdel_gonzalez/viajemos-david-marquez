@extends('welcome')

@section('header_scripts')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="col-md-12">
        <form class="form-inline" action="{{ route('historial.index') }}" method="GET" role="search">
            @csrf
            <div class="input-group">
                <select name="city_id" class="form-control" aria-describedby="search">
                    <option value="0">{{ __('messages.filter_by_city') }}</option>
                    @foreach($cities as $city)
                        <option value="{{ $city->id }}">{{ $city->name }}</option>
                    @endforeach
                </select>
            </div>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-search"></i>
            </button>
        </form>
    </div>
    <br />
    <div class="table-responsive">
        <table id="historial-table" class="display table table-hover">
            <thead>
                <th>{{ __('messages.record_id') }}</th>
                <th>{{ __('messages.city') }}</th>
                <th>{{ __('messages.country') }}</th>
                <th>{{ __('messages.humidity') }}</th>
                <th>{{ __('messages.date') }}</th>
            </thead>
            <tbody>
                @foreach($historial as $record)
                    <tr>
                        <td>{{ $record->id }}</td>
                        <td>{{ $record->city->name }}</td>
                        <td>{{ $record->city->country->name }}</td>
                        <td>{{ $record->humidity }}</td>
                        <td>{{ $record->created_at->toFormattedDateString() }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection

@section('footer_scripts')
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready( function () {
            $('#historial-table').DataTable();
        } );
    </script>
@endsection