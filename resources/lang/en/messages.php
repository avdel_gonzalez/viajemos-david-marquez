<?php 

return [
    'view_map' => 'View Map',
    'historial' => 'History',
    'map' => 'Map',
    'country' => 'Country',
    'select_city' => 'Select a city',
    'footer_text' => '2021 Viajemos.com . All rights reserved.',
    'city' => 'City',
    'timezone' => 'Timezone',
    'humidity' => 'Humidity',
    'date' => 'Search Date',
    'record_id' => 'Record id',
    'filter_by_city' => 'Filter by city',
    'change_language' => 'Change language',
];