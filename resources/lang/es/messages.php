<?php 

return [
    'view_map' => 'Ver mapa',
    'historial' => 'Historial',
    'map' => 'Mapa',
    'country' => 'País',
    'select_city' => 'Seleccione una ciudad',
    'footer_text' => '2021 Viajemos.com . Todos los derechos reservados.',
    'city' => 'Ciudad',
    'timezone' => 'Zona horaria',
    'humidity' => 'Humedad',
    'date' => 'Fecha de consulta',
    'record_id' => 'Id de registro',
    'filter_by_city' => 'Filtrar por ciudad',
    'change_language' => 'Cambiar idioma',
];