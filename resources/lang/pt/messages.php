<?php 

return [
    'view_map' => 'Ver mapa',
    'historial' => 'Registro',
    'map' => 'Mapa',
    'country' => 'País',
    'select_city' => 'Selecione uma cidade',
    'footer_text' => '2021 Viajemos.com . Todos os direitos reservados.',
    'city' => 'Cidade',
    'timezone' => 'Fuso horário',
    'humidity' => 'Umidade',
    'date' => 'Data de consulta',
    'record_id' => 'Registro',
    'filter_by_city' => 'Filtrar por cidade',
    'change_language' => 'Mudar idioma',
];