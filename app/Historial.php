<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historial extends Model
{
    protected $table = 'historial';

    protected $fillable = [
        'latitude',
        'longitude',
        'humidity',
        'city_id',
    ];

    public function city(){
        return $this->belongsTo('App\City');
    }

    //ScopeCity para filtro por ciudades
    public function scopeCityId($query, $city_id){
        //Valida que id de ciudad sea número y diferente a 0
        if(($city_id != '' && is_numeric($city_id) && $city_id != 0)){
            $query->where('city_id', $city_id);
        }
    }
}
