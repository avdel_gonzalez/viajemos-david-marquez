<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';

    protected $fillable = [
        'name',
        'iso_a2_code',
    ];

    public function cities(){
        return $this->hasMany('App\City');
    }
}
