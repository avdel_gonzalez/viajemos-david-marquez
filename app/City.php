<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = [
        'name',
        'code_name',
        'country_id',
    ];

    public function country(){
        return $this->belongsTo('App\Country');
    }

    public function historial(){
        return $this->hasMany('App\Historial');
    }
}
