<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\City;
use App\Historial;

class HomeController extends Controller
{
    public function index(){
        $cities = City::orderBy('id', 'asc')->with(['country'])->get();
        return view('map')->with(compact('cities'));
    }

    public function searchCity(Request $request){
        $messages = [
            'city_id_required' => __('messages.error_city_id_required'),
            'city_id_numeric' => __('messages.error_city_id_numeric'),
            'city_id_exits' => __('messages.error_city_id_exits'),
        ];

        $rules = [
            'city_id' => 'required|numeric|exists:cities,id',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            $errors = "";
            foreach($validator->errors()->messages() as $message){
                foreach($message as $error){
                    $errors .= "" . $error . "  //  ";
                }
            }
            //flash($errors)->error();
            return redirect()->route('studentProjects.create');
        } else {
            //Busca Ciudad en la base de datos, con su respectivo país
            $city = City::where('id', $request->city_id)->with(['country'])->first();
            //Arma parámetro de búsqueda
            $param = $city->code_name . ',' . $city->country->iso_a2_code;
            
            /* Fragmento de código extraído de https://developer.yahoo.com/weather/documentation.html#oauth-php */
            //Recolecta variables del archivo .env
            $url = env('WEATHER_API_URL'); 
            $app_id = env('WEATHER_APP_ID'); 
            $consumer_key = env('WEATHER_CONSUMER_KEY'); 
            $consumer_secret = env('WEATHER_CONSUMER_SECRET');

            //Arma query con parámetro de búsqueda
            $query =[
                'location' => $param,
                'format' => 'json',
            ];

            //Array de Autenticación OAuth 1.0
            $oauth = [
                'oauth_consumer_key' => $consumer_key,
                'oauth_nonce' => uniqid(mt_rand(1, 1000)),
                'oauth_signature_method' => 'HMAC-SHA1',
                'oauth_timestamp' => time(),
                'oauth_version' => '1.0'
            ];

            //Construye Url con Parámetros de búsqueda y Autenticación
            $base_info = $this->buildBaseString($url, 'GET', array_merge($query, $oauth));
            $composite_key = rawurlencode($consumer_secret) . '&';
            $oauth_signature = base64_encode(hash_hmac('sha1', $base_info, $composite_key, true));
            $oauth['oauth_signature'] = $oauth_signature;

            //Arma Array de Cabeceras
            $header = [
                $this->buildAuthorizationHeader($oauth),
                'X-Yahoo-App-Id: ' . $app_id
            ];

            //Array de Opciones
            $options = [
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_HEADER => false,
                CURLOPT_URL => $url . '?' . http_build_query($query),
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false
            ];

            //Inicio, ejecución y cierre de webservice
            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $response = curl_exec($ch);
            curl_close($ch);

            //Retornando datos en formato array
            $return_data = json_decode($response);
            /* Fin de gragmento de código de documentación oficial */

            //Escribiendo en el historial
            //Arma array de datos a guardar
            $historial_data = [
                'latitude' => $return_data->location->lat,
                'longitude' => $return_data->location->long,
                'humidity' => $return_data->current_observation->atmosphere->humidity,
                'city_id' => $city->id
            ];

            //Instancia modelo, Guarda registro
            $historial = new Historial($historial_data);
            $historial->save();

            //Armando array de datos necesarios para la vista
            $data_view = [
                'latitude' => $return_data->location->lat,
                'longitude' => $return_data->location->long,
                'city' => $return_data->location->city,
                'timezone_id' => $return_data->location->timezone_id,
                'humidity' => $return_data->current_observation->atmosphere->humidity,
            ];

            //demás ciudades para el buscador
            $cities = City::orderBy('id', 'asc')->with(['country'])->get();
            return view('city_details')->with(compact('data_view', 'cities'));
        }
    }

    function buildBaseString($baseURI, $method, $params) {
        $r = array();
        ksort($params);
        foreach($params as $key => $value) {
            $r[] = "$key=" . rawurlencode($value);
        }
        return $method . "&" . rawurlencode($baseURI) . '&' . rawurlencode(implode('&', $r));
    }
    
    function buildAuthorizationHeader($oauth) {
        $r = 'Authorization: OAuth ';
        $values = array();
        foreach($oauth as $key=>$value) {
            $values[] = "$key=\"" . rawurlencode($value) . "\"";
        }
        $r .= implode(', ', $values);
        return $r;
    }
}
