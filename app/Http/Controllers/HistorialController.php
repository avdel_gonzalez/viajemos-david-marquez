<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Historial;
use App\City;

class HistorialController extends Controller
{
    public function index(Request $request){
        //Busca registros de historial y trae la relación con ciudades
        $historial = Historial::cityId($request->get('city_id'))->orderBy('id', 'asc')->with('city')->get();
        //Itera registros y trae relaciones de ciudades con país
        foreach($historial as $record){
            $record->city->country;
        }
        //Busca ciudades para filstro de registros
        $cities = City::orderBy('id', 'asc')->get();
        //Retorna a la vista con variable historial
        return view('historial')->with(compact('historial', 'cities'));
    }
}
